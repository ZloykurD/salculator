﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Entities;
using Newtonsoft.Json;

namespace Calculator.Data.Database
{
    public class AppDBContext
    {
        private readonly List<Expression> _expressions;

        public AppDBContext()
        {
            _expressions = new List<Expression>();
        }

        public List<Expression> Expressions()
        {
            return _expressions;
        }
        
        public void Create(Expression expression)
        {
            if (expression == null)
            {
                throw new Exception(
                    $"Не удалось добавить сущность {nameof(Expressions)} с данными {JsonConvert.SerializeObject(expression)} ");
            }

            expression.Id = GetAutoIncrement();
            _expressions.Add(expression);
        }

        public void Remove(Expression expression)
        {
            if (expression == null)
            {
                throw new Exception(
                    $"Не удалось удалить сущность {nameof(Expressions)} с данными {JsonConvert.SerializeObject(expression)}");
            }

            if (_expressions.Count == 0)
            {
                throw new Exception(
                    $"Не удалось удалить сущность {nameof(Expressions)} с данными {JsonConvert.SerializeObject(expression)}, нет данных для уделения  ");
            }

            _expressions.Remove(expression);
        }

        public Expression Find(int id)
        {
            if (_expressions.Count > 0)
            {
                return _expressions.FirstOrDefault(x => x.Id == id);
            }

            return null;
        }
        
        private int GetAutoIncrement()
        {
            if (_expressions.Count > 0)
            {
                return _expressions.Max(x => x.Id) + 1;
            }

            return 0;
        }
    }
}
