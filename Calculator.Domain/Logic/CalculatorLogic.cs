﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Entities;
using Calculator.Domain.Enums;
using Calculator.Domain.Repositories;
using Calculator.Domain.Resource;
using static Calculator.Domain.Enums.Operants;
using static Calculator.Domain.Enums.OperantPosition;
using static Calculator.Domain.Resource.StringResource;

namespace Calculator.Domain.Logic
{
    public class CalculatorLogic
    {
        private static Repository _repository;
        private const int DIVIDE = 100;

        public CalculatorLogic()
        {
            _repository = new Repository();
        }

        public void Init()
        {
            Console.WriteLine($"\n\t{StringResource.Welcome}\n\n \t{StringResource.AppDescription}");
        }

        public double Calculate(double first, double second, int operant)
        {
            switch (operant)
            {
                default:
                case (int) PLUS:
                    return first + second;
                case (int) MINUS:
                    return first - second;
                case (int) MULTIPLICATION:
                    return first * second;
                case (int) DIVIDER:
                    return first / second;
            }
        }

        private void Devide()
        {
            foreach (Expression model in _repository.GetList().Where(x => x.Operant == (int) DIVIDER))
            {
                ShowModelResult(model);

                double res1 = (model.FirstDenominator * DIVIDE);
                double res2 = (model.SecondDenominator * DIVIDE);
                double res3 = CutToLength(res1 / res2);
                double res4 = ((int) res3 * res2);
                int index = 0;
                String digit = "";

                String first = FormatValue($"{res1}", res1.ToString().Length);
                String second = FormatValue($"{res2}", res2.ToString().Length);
                String therd = FormatValue($"{res3}", res3.ToString().Length);
                String four = FormatValue($"{res4}", res4.ToString().Length);


                double[] Xvalues = new double[res3.ToString().Length + 1];
                double[] Yvalues = new double[res3.ToString().Length + 1];


                String result = res3.ToString();
                foreach (char ch in result)
                {
                    digit = ch.ToString();

                    if (!digit.Equals(","))
                    {
                        if (index == 0)
                        {
                            double xxx = res2 * int.Parse(digit);

                            Xvalues[index] = xxx;


                            double yyy = res1 - xxx;
                            yyy *= 10;
                            Yvalues[index] = yyy;
                        }
                        else
                        {
                            double prevValue = Yvalues[index - 1];

                            double xxx = res2 * int.Parse(digit);

                            Xvalues[index] = xxx;


                            double yyy = prevValue - xxx;
                            int length = res3.ToString().Length - 2;
                            if (index < length)
                            {
                                yyy *= 10;
                            }

                            Yvalues[index] = yyy;
                        }

                        index++;
                    }
                }


                List<double> xValues = new List<double>();
                List<double> yValues = new List<double>();

                for (int i = 0; i < Xvalues.Length; i++)
                {
                    if (Xvalues[i] != 0 && i != 0)
                    {
                        xValues.Add(Xvalues[i]);
                    }
                }

                for (int i = 0; i < Yvalues.Length; i++)
                {
                    if (Yvalues[i] != 0)
                    {
                        yValues.Add(Yvalues[i]);
                    }
                }

                Print("\n\n\n\n");
                Print($"\t{first}|{second}");
                Print($"\t-   |-------");
                Print($"\t{four}|{therd}");


                for (int i = 0; i < index; i++)
                {
                    Print($"\t ----");

                    if (i <= yValues.Count - 1)
                    {
                        Print($"\t {yValues[i]}");
                    }

                    if (i <= xValues.Count - 1)
                    {
                        Print($"\t -   ");
                        Print($"\t {xValues[i]}");
                    }
                }
            }
        }

        private void Multiply()
        {
            foreach (Expression model in _repository.GetList().Where(x => x.Operant == (int) MULTIPLICATION))
            {
                ShowModelResult(model);

                Print(
                    $"\t |{FormatValue(model.FirstDenominator.ToString(), model.FirstDenominator.ToString().Length)}");
                Print($"\t{ConvertOperantToString(model.Operant)}|-------");
                Print(
                    $"\t |{FormatValue(model.SecondDenominator.ToString(), model.SecondDenominator.ToString().Length)}");
                Print($"\t  {model.Result}");
                Print($"\t");
            }
        }

        private void Plus()
        {
            foreach (Expression model in _repository.GetList().Where(x => x.Operant == (int) PLUS))
            {
                ShowModelResult(model);
                Print(
                    $"\t |{FormatValue(model.FirstDenominator.ToString(), model.FirstDenominator.ToString().Length)}");
                Print($"\t{ConvertOperantToString(model.Operant)}|-------");
                Print(
                    $"\t |{FormatValue(model.SecondDenominator.ToString(), model.SecondDenominator.ToString().Length)}");
                Print($"\t  {model.Result}");
                Print($"\t");
            }
        }

        private void Minus()
        {
            foreach (Expression model in _repository.GetList().Where(x => x.Operant == (int) MINUS))
            {
                ShowModelResult(model);
                Print(
                    $"\t |{FormatValue(model.FirstDenominator.ToString(), model.FirstDenominator.ToString().Length)}");
                Print($"\t{ConvertOperantToString(model.Operant)}|-------");
                Print(
                    $"\t |{FormatValue(model.SecondDenominator.ToString(), model.SecondDenominator.ToString().Length)}");
                Print($"\t  {model.Result}");
                Print($"\t");
            }
        }

        public void Start()
        {
            Expression expression = new Expression();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Print(Border);
            String input = String.Empty;

            Print($"{EnterFirstOperant}");

            HandleParam(FIRST, out input);
            expression.FirstDenominator = Convert.ToDouble(input.Replace(".", ","));

            Print($"{EnterOperant}");
            HandleParam(OPERANT, out input);
            expression.Operant = Convert.ToInt32(ConvertOperantToInt(input));

            Print($"{EnterSecondOperant}");
            HandleParam(SECOND, out input);
            expression.SecondDenominator = Convert.ToDouble(input.Replace(".", ","));

            Print(Border);

            expression.Result =
                Calculate(expression.FirstDenominator, expression.SecondDenominator, expression.Operant);
            _repository.Create(expression);

            ShowResult(expression.Operant);

            Print(PressAnyKeyToContinue);
            Console.ReadKey();
            Console.Clear();
        }

        private void HandleParam(OperantPosition position, out String values)
        {
            bool isDone = false;
            String value = String.Empty;
            while (!isDone)
            {
                value = Console.ReadLine();
                isDone = ValidateInput(value, position);
            }

            values = value;
        }

        private bool ValidateInput(String value, OperantPosition position)
        {
            if (value == String.Empty)
            {
                Print(ErrorInputValue);
                return false;
            }

            switch (position)
            {
                case FIRST:
                case SECOND:
                    return DoubleValidate(value);
                case OPERANT:
                    return OperantValidate(value);
                default:
                    return false;
            }
        }

        private bool DoubleValidate(String value)
        {
            try
            {
                double secondValue = Double.Parse(value.Replace(".", ","));
                if (secondValue > 0)
                {
                    return true;
                }
                else if (secondValue == 0)
                {
                    Print(ErrorInputValue);
                    return false;
                }
            }
            catch (Exception e)
            {
                Print(e.Message);
                return false;
            }

            return false;
        }

        private bool OperantValidate(String value)
        {
            foreach (String operant in OperantList)
            {
                if (operant.Equals(value))
                {
                    return true;
                }
            }

            Print(ErrorInputOperant);
            return false;
        }

        private int ConvertOperantToInt(String input)
        {
            switch (input)
            {
                default:
                case "+":
                    return (int) PLUS;
                case "*":
                    return (int) MULTIPLICATION;
                case "/":
                    return (int) DIVIDER;
                case "-":
                    return (int) MINUS;
            }
        }

        private String ConvertOperantToString(int operant)
        {
            switch (operant)
            {
                default:
                case (int) PLUS:
                    return "+";
                case (int) MULTIPLICATION:
                    return "*";
                case (int) DIVIDER:
                    return "/";
                case (int) MINUS:
                    return "-";
            }
        }

        private static void Print(String value = "")
        {
            Console.WriteLine(value);
        }

        private static String FormatValue(String value, int length)
        {
            switch (length)
            {
                case 1:
                    return $"___{value}";
                case 3:
                    return $"_{value}";
                case 2:
                    return $"__{value}";
                default:
                case 4:
                    return $"{value}";
            }
        }

        private double CutToLength(double x, int precision = 3)
        {
            return ((int) (x * Math.Pow(10.0, precision)) / Math.Pow(10.0, precision));
        }

        public void ShowResult(int Operant)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            Print(StringResource.Border);

            switch (Operant)
            {
                default:
                case (int) PLUS:
                    Plus();
                    break;
                case (int) MINUS:
                    Minus();
                    break;
                case (int) MULTIPLICATION:
                    Multiply();
                    break;
                case (int) DIVIDER:
                    Devide();
                    break;
            }

            Print(StringResource.Border);
        }

        private void ShowModelResult(Expression model)
        {
            Print(
                $"\n Решение №{model.Id}" +
                $"\n Первый знаменатель {model.FirstDenominator}" +
                $"\n Оперант {ConvertOperantToString(model.Operant)}" +
                $"\n Второй знаменатель {model.SecondDenominator}" +
                $"\n Результат {model.Result}\n\n ");

            Print(
                $"\t\t{model.FirstDenominator} {ConvertOperantToString(model.Operant)} {model.SecondDenominator} = {model.Result}\n\n");
        }
    }
}