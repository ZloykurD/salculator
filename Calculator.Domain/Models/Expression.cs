﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Domain.Models
{
    public class Expression
    {
        public int Id { get; set; }
        public double FirstDenominator { get; set; }
        public int Operant { get; set; }
        public double SecondDenominator { get; set; }
        public double Result { get; set; }
    }
}