﻿using System;
using System.Collections.Generic;
using Expression = Calculator.Data.Entities.Expression;
namespace Calculator.Domain.Repositories
{
    public interface IRepository
    {
        List<Expression> GetList();
        void Create(Expression entity);
        void Delete(int id);
        Expression Find(int id);
    }
}