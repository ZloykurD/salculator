﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Database;
using Calculator.Data.Entities;
using Newtonsoft.Json;
using Expression = Calculator.Data.Entities.Expression;

namespace Calculator.Domain.Repositories
{
    public class Repository : IRepository
    {
        private readonly AppDBContext db;

        public Repository()
        {
            db = new AppDBContext();
        }

        public List<Expression> Expressions { get; set; } = new List<Expression>();

        public List<Expression> GetList()
        {
            return db.Expressions();
        }

        public void Create(Expression entity)
        {
            Expression item = entity;
          
            try
            {
                db.Create(item);
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    $"Не удалось создать сущность {nameof(Expression)} с данными {JsonConvert.SerializeObject(item)} {Environment.NewLine}{ex}");
                throw;
            }
        }

        public void Delete(int id)
        {
            Expression item = null;
            try
            {
                item = db.Find(id);

                if (item == null)
                {
                    throw new Exception();
                }

                db.Remove(item);
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    $"Не удалось удалить сущность {nameof(Expression)} с данными {JsonConvert.SerializeObject(item)} {Environment.NewLine}{ex}");
                throw;
            }
        }

        public Expression Find(int id)
        {
            Expression item;
            try
            {
                item = db.Find(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    $"Не удалось найти сущность {nameof(Expression)} по id {id} {Environment.NewLine}{ex}");
                throw;
            }

            return item;
        }
    }
}