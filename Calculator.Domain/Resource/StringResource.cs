﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Domain.Resource
{
   public class StringResource
   {
       public static String Welcome = "Добро пожаловать!!!";
       public static String AppDescription = "Калькулятор для сложения, вычитания, умножения и деления столбиком";
       public static String Border = "----------------------------------------------------------------------";


       public static String EnterFirstOperant = "\tВведите первое число";
       public static String EnterSecondOperant = "\tВведите второе число";
       public static String EnterOperant = "\tВведите оперант \t(пример: + , - , * , / )";

      public static String ErrorValueIsEmpty = "Необходимо ввести данные!";
      public static String ErrorInputValue = "Необходимо ввести корректные данные! \n значение не может быть пустым или равно нулю";
      public static String ErrorInputOperant = "Необходимо ввести корректные данные! \n \t(пример: + , - , * , / )";
      public static String[] OperantList = new []{"+","-","*","/"};


       public static String PressAnyKeyToContinue = "Для повторного ввода нажмите любую клавишу.";

   }
}
