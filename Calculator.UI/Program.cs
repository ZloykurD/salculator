﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Calculator.Domain.Logic;
using Calculator.Domain.Models;


namespace Calculator.UI
{
    public class Program
    {
        private static CalculatorLogic _calculator = null;
        private static bool _isAcive;

        public static void Main(string[] args)
        {
            Init();
            
            while (_isAcive)
            {
                try
                {
                    _calculator.Start();
                }
                catch (Exception e)
                {
                    _isAcive = false;
                    Console.WriteLine($"Ошибка: {e.Message}");
                }
            }
            
            Console.ReadKey();
        }
        
        public static void Init()
        {
            _isAcive = true;
            _calculator = new CalculatorLogic();
            _calculator.Init();
        }
    }
}